# File to create and modify the graphical user interface

from Tkinter import *
import pirate_bot as pb
# creates main window that opens first when the program is run
class MainWindow:
    def __init__(self, master):
        self.master = master
        self.retweet_button = Button(master, text="Retweet", command=self.rf)
        self.favorite_button = Button(master, text="Favorite", command=self.rf)
        self.follow_button = Button(master, text="Follow", command=self.follow)
        self.send_button = Button(master, text="Send a pirate tweet", command=self.send)
        self.random_button = Button(master, text="Generate random pirate tweet", command=self.generate)
        self.photo_button = Button(master, text="Tweet photo", command=self.photo)
        self.translate_button = Button(master, text="Translate", command=self.translate)

        self.retweet_button.grid(row=1, column=1)
        self.favorite_button.grid(row=2, column=1)
        self.follow_button.grid(row=3, column=1)
        self.send_button.grid(row=4, column=1)
        self.random_button.grid(row=5, column=1)
        self.photo_button.grid(row=6, column=1)
        self.translate_button.grid(row=7, column=1)

        # Displays and binds button, so when clicked, the enter_button funtion is called



    # enter_button method is the command for when the enter button is pressed on the
    # main window.  Depending on what is typed in, the corresponding window will appear
    def rf(self):
        top = Toplevel()
        #rf_gui = RFWindow(top)
        top.mainloop()
    def follow(self):
        top = Toplevel()
        follow_gui = FollowWindow(top)
        top.mainloop()
    def send(self):
        top = Toplevel()
        send_gui = SendWindow(top)
        top.mainloop()
    def generate(self):
        pb.generate_pirate_tweet()
    def photo(self):
            pb.tweet_photo()
    def translate(self):
        top = Toplevel()
        #translate_gui = TranslateWindow(top)
        top.mainloop()



class SendWindow:
    def __init__(self, master):
        self.master = master
        self.prompt = Label(master, text="Enter the contents of your tweet:")

        self.user_input = StringVar()
        self.entry = Entry(master, textvariable=self.user_input)

        self.send_button = Button(master, text="Send", command=self.send_tweet)

        self.prompt.grid(row=1, column=1)
        self.entry.grid(row=1, column=2)
        self.send_button.grid(row=1, column=3)

    def send_tweet(self):
        pb.send_tweet(self.user_input.get())

class FollowWindow:
    def __init__(self, master):
        self.master = master
        self.prompt = Label(master, text="Enter the username you want to follow")

        self.user_input = StringVar()
        self.entry = Entry(master, textvariable=self.user_input)

        self.follow_button = Button(master, text="Follow", command=self.follow)

        self.prompt.grid(row=1, column=1)
        self.entry.grid(row=1, column=2)
        self.follow_button.grid(row=1, column=3)

    def follow(self):
        pb.follow_user(self.user_input.get())



def main():
    root = Tk()
    app = MainWindow(root)
    root.mainloop()

if __name__ == '__main__':
    main()

